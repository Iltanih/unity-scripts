﻿using System.Threading;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour
{
    [SerializeField]

    private Vector3 direction;
   
    public float speed;
    public float speedrot; //velocidad de la rotacion

    // Start is called before the first frame update
    void Update()
    {
        direction = ClampVector3(direction); // trans
        transform.Translate (direction * (speed * Time.deltaTime));
        transform.Rotate(Vector3.up * (Time.deltaTime * speedrot));
    }

    // Update is called once per frame
    public static Vector3 ClampVector3(Vector3 target)
    {
        float clampedX = Mathf.Clamp(target.x, -1f, 1f);
        float clampedY = Mathf.Clamp(target.y, -1f, 1f);
        float clampedZ = Mathf.Clamp(target.z, -1f, 1f);


        Vector3 result = new Vector3(clampedX, clampedY, clampedZ);
        return result;

    }
}
