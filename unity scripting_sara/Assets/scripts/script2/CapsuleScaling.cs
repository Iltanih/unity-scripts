﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes; // posibles ejes de escalado
    public float scaleUnits; //VELOCIDAD DEL ESCALADO


    // Update is called once per frame
    void Update()
    {
        axes = CapsuleMovement.ClampVector3(axes); // escala
        transform.localScale += axes * (scaleUnits * Time.deltaTime);

    }
}
